const path = require('path');
function resolve(dir) {
    return path.join(__dirname, '..', dir);
}

module.exports = {
    publicPath: './',
    indexPath: 'index.html',
    pages: {
        index: {
            entry: 'examples/main.js',
            template: 'public/index.html',
            filename: 'index.html'
        }
    }, 
    configureWebpack: config => {
        config.devtool = "source-map";
    },
    chainWebpack: config => { 
        config.resolve.alias
            .set("vue$",'vue/dist/vue.esm.js');
        config.module
            .rule('js')
            .include
                .add(resolve('packages'))
                .end()
            .use('bable')
                .loader('bable-loader')
                .tap(option => {
                    return option
                })
        config.module
            .rule('js')
            .include
                .add(resolve('packages'))
                .end()
            .use('bable')
                .loader('bable-loader')
                .tap(option => {
                    return option
                })
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "./examples/style/all.scss"; `
            }
        }
    }
}