import YInputNumber from './src/inputNumber.vue';
YInputNumber.install = function (Vue) {
    Vue.component(YInputNumber.name, YInputNumber);
}

export default YInputNumber;