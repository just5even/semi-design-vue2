import YRadio from "./src/radio.vue";

YRadio.install = function(Vue) {
    Vue.component(YRadio.name, YRadio);
}

export default YRadio;