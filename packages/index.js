import YButton from './button/src/button.vue';
import YButtonGroup from './bottonGroup/src/buttonGroup.vue';
import SpinIcon from './spinIcon/src/spinIcon.vue';
import YInput from './input/src/input.vue';
import YTextarea from './textarea/src/textarea.vue';
import IconClear from './icon/clearBtn/src/clearBtn.vue';
import YInputNumber from './inputNumber/src/inputNumber.vue';
import IconChevronDown from './icon/chevronDown/src/chevronDown.vue';
import IconChevronUp from './icon/chevronUp/src/chevronUp.vue';
import YCheckbox from './checkbox/src/checkbox.vue';
import YCheckboxGroup from './checkGroup/src/checkboxGroup.vue';
import YRadio from './radio/src/radio.vue';
import YRadioGroup from './radioGroup/src/radioGroup.vue';
import YSelect from './select/src/select.vue';
import YTag from './tag/src/tag.vue';
import YAvatar from './avatar/src/avatar.vue';
import YAvatarGroup from './avatarGroup/src/avatarGroup.vue';
import YTooltip from './tooltip/src/tooltip.vue';
import YTeleport from './teleport/src/teleport.vue';

const components = [
    YButton,
    YButtonGroup,
    SpinIcon,
    YInput,
    YTextarea,
    IconClear,
    YInputNumber,
    IconChevronUp,
    IconChevronDown,
    YCheckbox,
    YCheckboxGroup,
    YRadio,
    YRadioGroup,
    YSelect,
    YTag,
    YAvatar,
    YAvatarGroup,
    YTooltip,
    YTeleport
]


const install = function(Vue) {
    if(install.installed) return;
    components.map(component => Vue.component(component.name, component));
}

if(typeof Window !== 'undefined' && Window.Vue) {
    install(Window.Vue);
}

export default {
    install,
    YButton,
    YButtonGroup,
    SpinIcon,
    YInput,
    YTextarea,
    IconClear,
    YInputNumber,
    IconChevronDown,
    IconChevronUp,
    YCheckbox,
    YCheckboxGroup,
    YRadio,
    YRadioGroup,
    YSelect,
    YTag,
    YAvatar,
    YAvatarGroup,
    YTooltip,
    YTeleport
}