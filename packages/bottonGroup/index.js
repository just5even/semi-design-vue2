import YButtonGroup from "./src/buttonGroup.vue";

YButtonGroup.install = function(Vue) {
    Vue.component(YButtonGroup.name, YButtonGroup);
}

export default YButtonGroup;