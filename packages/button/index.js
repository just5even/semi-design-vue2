import YButton from "./src/button.vue";

YButton.install = function(Vue) {
    Vue.component(YButton.name, YButton);
}

export default YButton;