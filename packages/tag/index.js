import YTag from "./src/tag.vue";

YTag.install = function(Vue) {
    Vue.component(YTag.name, YTag);
}

export default YTag;