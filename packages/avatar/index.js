import YAvatar from "./src/avatar.vue";

YAvatar.install = function(Vue) {
    Vue.component(YAvatar.name, YAvatar);
}

export default YAvatar;