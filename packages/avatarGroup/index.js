import YAvatarGroup from "./src/avatarGroup.vue";

YAvatarGroup.install = function(Vue) {
    Vue.component(YAvatarGroup.name, YAvatarGroup);
}

export default YAvatarGroup;