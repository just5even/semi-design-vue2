import YSelect from "./src/select.vue";

YSelect.install = function(Vue) {
    Vue.component(YSelect.name, YSelect);
}

export default YSelect;