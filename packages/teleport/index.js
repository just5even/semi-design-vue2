import YTeleport from "./src/teleport.vue";

YTeleport.install = function(Vue) {
    Vue.component(YTeleport.name, YTeleport);
}

export default YTeleport;