import SpinIcon from "./src/spinIcon.vue";

SpinIcon.install = function(Vue) {
    Vue.component(SpinIcon.name, SpinIcon);
}

export default SpinIcon;