import YCheckbox from "./src/checkbox.vue";

YCheckbox.install = function(Vue) {
    Vue.component(YCheckbox.name, YCheckbox);
}

export default YCheckbox;