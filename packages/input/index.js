import YInput from './src/input.vue';
YInput.install = function (Vue) {
    Vue.component(YInput.name, YInput);
}

export default YInput;