import YTooltip from "./src/tooltip.vue";

YTooltip.install = function(Vue) {
    Vue.component(YTooltip.name, YTooltip);
}

export default YTooltip;