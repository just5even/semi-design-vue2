import YRadioGroup from "./src/radioGroup.vue.vue";

YRadioGroup.install = function(Vue) {
    Vue.component(YRadioGroup.name, YRadioGroup);
}

export default YRadioGroup;