import IconCheckboxIndeteminate from './src/checkboxIndeteminate.vue';
IconCheckboxIndeteminate.install = function (Vue) {
    Vue.component(IconCheckboxIndeteminate.name, IconCheckboxIndeteminate);
}

export default IconCheckboxIndeteminate;