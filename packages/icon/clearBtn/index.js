import IconClear from './src/clearBtn.vue';
IconClear.install = function (Vue) {
    Vue.component(IconClear.name, IconClear);
}

export default IconClear;