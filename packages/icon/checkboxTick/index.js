import IconCheckboxTick from './src/checkboxTick.vue';
IconCheckboxTick.install = function (Vue) {
    Vue.component(IconCheckboxTick.name, IconCheckboxTick);
}

export default IconCheckboxTick;