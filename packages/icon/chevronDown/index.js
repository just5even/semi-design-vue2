import IconChevronDown from './src/chevronDown.vue';
IconChevronDown.install = function (Vue) {
    Vue.component(IconChevronDown.name, IconChevronDown);
}

export default IconChevronDown;