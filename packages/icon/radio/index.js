import IconRadio from './src/radio.vue';
IconRadio.install = function (Vue) {
    Vue.component(IconRadio.name, IconRadio);
}

export default IconRadio;