import IconChevronUp from './src/chevronUp.vue';
IconChevronUp.install = function (Vue) {
    Vue.component(IconChevronUp.name, IconChevronUp);
}

export default IconChevronUp;