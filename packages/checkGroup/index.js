import YCheckboxGroup from "./src/checkboxGroup.vue";

YCheckboxGroup.install = function(Vue) {
    Vue.component(YCheckboxGroup.name, YCheckboxGroup);
}

export default YCheckboxGroup;