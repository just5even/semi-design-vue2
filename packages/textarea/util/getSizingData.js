import { pick } from 'lodash';
const SIZING_STYLE = [
    'borderBottomWidth',
    'borderLeftWidth',
    'borderRightWidth',
    'borderTopWidth',
    'boxSizing',
    'fontFamily',
    'fontSize',
    'fontStyle',
    'fontWeight',
    'letterSpacing',
    'lineHeight',
    'paddingBottom',
    'paddingLeft',
    'paddingRight',
    'paddingTop',
    // non-standard
    'tabSize',
    'textIndent',
    // non-standard
    'textRendering',
    'textTransform',
    'width',
];


export default function getSizingData(node) {
    const style = window.getComputedStyle(node);
    if(style === null) return;

    const sizingStyle = pick(style, SIZING_STYLE);
    const { boxSizing } = sizingStyle;
 
    if(boxSizing === '') return;

    const paddingSize = 
        parseFloat(sizingStyle.paddingBottom) + 
        parseFloat(sizingStyle.paddingTop);

    const borderSize = 
        parseFloat(sizingStyle.borderBottomWidth) + 
        parseFloat(sizingStyle.borderTopWidth);

    return {
        sizingStyle,
        paddingSize,
        borderSize
    }
}