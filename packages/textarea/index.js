import YTextarea from "./src/textarea.vue";

YTextarea.install = function(Vue) {
    Vue.component(YTextarea.name, YTextarea);
}

export default YTextarea;