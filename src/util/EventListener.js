const EventListener = {
    listen(target, eventType, callBack) {
        if(target.addEventListener) {
            target.addEventListener(eventType, callBack, false);
            return {
                remove() {
                    target.removeEventListener(eventType, callBack, false);
                }
            }
        } else{
            target.attachEvent('on' + eventType, callBack);
            return {
                remove() {
                    target.detachEvent('on' + eventType, callBack);
                }
            }
        }
    }
}

export default EventListener;