(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["yui"] = factory();
	else
		root["yui"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "8875":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// addapted from the document.currentScript polyfill by Adam Miller
// MIT license
// source: https://github.com/amiller-gh/currentScript-polyfill

// added support for Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1620505

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
}(typeof self !== 'undefined' ? self : this, function () {
  function getCurrentScript () {
    var descriptor = Object.getOwnPropertyDescriptor(document, 'currentScript')
    // for chrome
    if (!descriptor && 'currentScript' in document && document.currentScript) {
      return document.currentScript
    }

    // for other browsers with native support for currentScript
    if (descriptor && descriptor.get !== getCurrentScript && document.currentScript) {
      return document.currentScript
    }
  
    // IE 8-10 support script readyState
    // IE 11+ & Firefox support stack trace
    try {
      throw new Error();
    }
    catch (err) {
      // Find the second match for the "at" string to get file src url from stack.
      var ieStackRegExp = /.*at [^(]*\((.*):(.+):(.+)\)$/ig,
        ffStackRegExp = /@([^@]*):(\d+):(\d+)\s*$/ig,
        stackDetails = ieStackRegExp.exec(err.stack) || ffStackRegExp.exec(err.stack),
        scriptLocation = (stackDetails && stackDetails[1]) || false,
        line = (stackDetails && stackDetails[2]) || false,
        currentLocation = document.location.href.replace(document.location.hash, ''),
        pageSource,
        inlineScriptSourceRegExp,
        inlineScriptSource,
        scripts = document.getElementsByTagName('script'); // Live NodeList collection
  
      if (scriptLocation === currentLocation) {
        pageSource = document.documentElement.outerHTML;
        inlineScriptSourceRegExp = new RegExp('(?:[^\\n]+?\\n){0,' + (line - 2) + '}[^<]*<script>([\\d\\D]*?)<\\/script>[\\d\\D]*', 'i');
        inlineScriptSource = pageSource.replace(inlineScriptSourceRegExp, '$1').trim();
      }
  
      for (var i = 0; i < scripts.length; i++) {
        // If ready state is interactive, return the script tag
        if (scripts[i].readyState === 'interactive') {
          return scripts[i];
        }
  
        // If src matches, return the script tag
        if (scripts[i].src === scriptLocation) {
          return scripts[i];
        }
  
        // If inline source matches, return the script tag
        if (
          scriptLocation === currentLocation &&
          scripts[i].innerHTML &&
          scripts[i].innerHTML.trim() === inlineScriptSource
        ) {
          return scripts[i];
        }
      }
  
      // If no match, return null
      return null;
    }
  };

  return getCurrentScript
}));


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (true) {
    var getCurrentScript = __webpack_require__("8875")
    currentScript = getCurrentScript()

    // for backward compatibility, because previously we directly included the polyfill
    if (!('currentScript' in document)) {
      Object.defineProperty(document, 'currentScript', { get: getCurrentScript })
    }
  }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"b4d2baae-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/button/src/button.vue?vue&type=template&id=3d268c71&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{ref:"bottonRef",class:[
    _vm.prefix,
    _vm.disabled ? _vm.prefix + '-disabled' : '',
    _vm.prefix + '-' + _vm.theme,
    _vm.disabled ? '' : _vm.prefix + '-' + _vm.type,
    _vm.prefix + '-size-' + _vm.size,
    _vm.block ? _vm.prefix + '-block' : '',
    _vm.loading || _vm.$slots.icon ? _vm.prefix + '-with-icon' : '',
    _vm.loading ? _vm.prefix + '-loading' : '' ],attrs:{"disabled":_vm.disabled},on:{"click":_vm.handleClick}},[_c('span',{class:[_vm.prefix + '-content']},[(_vm.loading && _vm.iconPosition === 'left')?[_c('spinIcon')]:_vm._e(),(!_vm.loading && _vm.$slots.icon && _vm.iconPosition === 'left')?_c('span',{class:[
            'semi-icon', 
            'semi-icon-default',
            _vm.$slots.default && _vm.ariaLabel ? 'semi-with-icon-only' : ''
            ]},[_vm._t("icon")],2):_vm._e(),(!_vm.ariaLabel && _vm.$slots.default)?_c('span',{class:[
            _vm.$slots.icon || _vm.loading ? _vm.iconPosition === 'left' ? _vm.prefix + '-content-right' : _vm.prefix + '-content-left' : ''
        ]},[_vm._t("default")],2):_vm._e(),(_vm.ariaLabel && !_vm.$slots.default)?_c('span',[_vm._v(_vm._s(_vm.ariaLabel))]):_vm._e(),(_vm.loading && _vm.iconPosition === 'right')?[_c('spinIcon')]:_vm._e(),(!_vm.loading && _vm.$slots.icon && _vm.iconPosition === 'right')?_c('span',{class:[
            'semi-icon', 
            'semi-icon-default',
            _vm.$slots.default && _vm.ariaLabel ? 'semi-with-icon-only' : ''
            ]},[_vm._t("icon")],2):_vm._e()],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./packages/button/src/button.vue?vue&type=template&id=3d268c71&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"b4d2baae-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/spinIcon/src/spinIcon.vue?vue&type=template&id=cb8bf416&
var spinIconvue_type_template_id_cb8bf416_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{attrs:{"width":"48","height":"48","viewBox":"0 0 36 36","version":"1.1","xmlns":"http://www.w3.org/2000/svg"}},[_c('defs',[_c('linearGradient',{attrs:{"x1":"0%","y1":"100%","x2":"100%","y2":"100%","id":_vm.id}},[_c('stop',{attrs:{"stop-color":"currentColor","stop-opacity":"0","offset":"0%"}}),_c('stop',{attrs:{"stop-color":"currentColor","stop-opacity":"0.50","offset":"39.9430698%"}}),_c('stop',{attrs:{"stop-color":"currentColor","offset":"100%"}})],1)],1),_c('g',{attrs:{"id":"Artboard","stroke":"none","stroke-width":"4","fill":"none","fill-rule":"evenodd"}},[_c('rect',{attrs:{"id":"Rectangle","fill-opacity":"0.01","fill":"#FFFFFF","x":"0","y":"0","width":"36","height":"36"}}),_c('path',{attrs:{"d":"M34,18 C34,9.163444 26.836556,2 18,2 C11.6597233,2 6.18078805,5.68784135 3.59122325,11.0354951","id":"Path","stroke":'url(#' + _vm.id + ')',"stroke-width":"4","stroke-linecap":"round"}})])])}
var spinIconvue_type_template_id_cb8bf416_staticRenderFns = []


// CONCATENATED MODULE: ./packages/spinIcon/src/spinIcon.vue?vue&type=template&id=cb8bf416&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/spinIcon/src/spinIcon.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var spinIconvue_type_script_lang_js_ = ({
    name: 'SpinIcon',
    data() {
        return {
            id: -1
        }
    },
    beforeMount() {
        let stamp = new Date().getTime();
        this.id = 'linearGradient' + (((1+Math.random())*stamp)|0).toString(16)
    }
});

// CONCATENATED MODULE: ./packages/spinIcon/src/spinIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var src_spinIconvue_type_script_lang_js_ = (spinIconvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./packages/spinIcon/src/spinIcon.vue





/* normalize component */

var component = normalizeComponent(
  src_spinIconvue_type_script_lang_js_,
  spinIconvue_type_template_id_cb8bf416_render,
  spinIconvue_type_template_id_cb8bf416_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var spinIcon = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/button/src/button.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var buttonvue_type_script_lang_js_ = ({
    name: 'YButton', 
    components: { spinIcon: spinIcon },
    data() {
        return {
            prefix: 'semi-button'
        }
    },
    props: {
        ariaLabel: {
            default: '',
            type: String
        },
        disabled: Boolean,
        icon: {
            default: '',
            type: String
        },
        iconPosition: {
            default: 'left', // 可选值 left | right
            type: String
        },
        loading: Boolean,
        size: {
            default: 'default', // 可选值 large | default | samll
            type: String
        },
        theme: {
            default: 'light', // 可选值 solid (有背景色) | borderless (无背景色) | light (浅背景色)
            type: String
        },
        type: {
            default: 'primary', // 可选值 primary | secondary | tertiary | waring | danger
            type: String
        },
        block: Boolean
    },
    methods: {
        handleClick(evt) {
            this.$emit('click', evt);
        }
    }
});

// CONCATENATED MODULE: ./packages/button/src/button.vue?vue&type=script&lang=js&
 /* harmony default export */ var src_buttonvue_type_script_lang_js_ = (buttonvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./packages/button/src/button.vue





/* normalize component */

var button_component = normalizeComponent(
  src_buttonvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var src_button = (button_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"b4d2baae-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/bottonGroup/src/buttonGroup.vue?vue&type=template&id=0813f84a&
var buttonGroupvue_type_template_id_0813f84a_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:_vm.prefix + '-group'},[_vm._t("default")],2)}
var buttonGroupvue_type_template_id_0813f84a_staticRenderFns = []


// CONCATENATED MODULE: ./packages/bottonGroup/src/buttonGroup.vue?vue&type=template&id=0813f84a&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/bottonGroup/src/buttonGroup.vue?vue&type=script&lang=js&
//
//
//
//
//
//

/* harmony default export */ var buttonGroupvue_type_script_lang_js_ = ({
    name: "YButtonGroup",
    data() {
        return {
            prefix: 'semi-button'
        }
    },
    mounted() {
        console.log(this.$slots.default);
    }
});

// CONCATENATED MODULE: ./packages/bottonGroup/src/buttonGroup.vue?vue&type=script&lang=js&
 /* harmony default export */ var src_buttonGroupvue_type_script_lang_js_ = (buttonGroupvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./packages/bottonGroup/src/buttonGroup.vue





/* normalize component */

var buttonGroup_component = normalizeComponent(
  src_buttonGroupvue_type_script_lang_js_,
  buttonGroupvue_type_template_id_0813f84a_render,
  buttonGroupvue_type_template_id_0813f84a_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var buttonGroup = (buttonGroup_component.exports);
// CONCATENATED MODULE: ./packages/index.js




const components = [
    src_button,
    buttonGroup,
    spinIcon
]


const install = function(Vue) {
    if(install.installed) return;
    components.map(component => Vue.component(component.name, component));
}

if(typeof Window !== 'undefined' && Window.Vue) {
    install(Window.Vue);
}

/* harmony default export */ var packages_0 = ({
    install,
    YButton: src_button,
    YButtonGroup: buttonGroup,
    SpinIcon: spinIcon
});
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (packages_0);



/***/ })

/******/ });
});
//# sourceMappingURL=yui.umd.js.map